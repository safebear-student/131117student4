import base_test

class TestCases(base_test.BaseTest):

    def test_01_test_login(self):
        # step 1: check we're on the welcome page
        assert self.welcomepage.check_page()

        #step 2: click on login and the login page loads
        assert self.welcomepage.click_login(self.loginpage)

        #step 3: login to the webpage and confirm the main page appears
        assert self.loginpage.login(self.mainpage, "testuser", "testing")

        #step 4: login to the webpage and confirm main page appears, then logout
        assert self.mainpage.click_logout(self.welcomepage)

